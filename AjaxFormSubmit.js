<script type="application/javascript">
    $(document).ready(function () {
        $("button#submit").click(function(e){
            e.preventDefault();
            if ($("#frmRepresentative").valid()) {
                var $form = $("#frmRepresentative");
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serialize(),
                    success: function (data, status) {
                        if(data.toastr_error){
                            toastr[data.toastr_error](data.message, data.title);
                            return;
                        }

                        toastr[data.toastr_success](data.message, data.title);
                        $('#myModal').modal('hide');
                    },
                    error: function (result) {
                        if( result.status === 401 ) { //redirect if not authenticated user.
                            //$( location ).prop( 'pathname', 'auth/login' );
                        }
                        if( result.status === 422 ) {
                            var errors = $.parseJSON(result.responseText);
                            errorsHtml = '<div><ul>'; //class="alert alert-danger"
                            $.each( errors, function( key, value ) {
                                errorsHtml += '<li>' + value[0] + '</li>';
                            });
                            errorsHtml += '</ul></div>';
                            toastr["error"](errorsHtml, "Invalid/Duplicate Input Data");
                        }
                    }
                });
            }

        });
    });
</script>