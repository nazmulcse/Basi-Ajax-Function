<script>
$(document).ready(function(){	
		
    $(".additionBeneficial").keyup(function(){
       //console.log(this);
	   var str = "additional_beneficiary_";
	   var len = str.length;	   
	   var id = $(this).attr('id');
	   var index = id.substring(len); 
       //console.log(index);
	   
   	   var monthlyAmount = $("#hidAmount").val();
	   monthlyAmount = (monthlyAmount) ? monthlyAmount : 1;
	   
       var numberOfMonth = $("#num_installment_month").val();
	   numberOfMonth = (numberOfMonth) ? numberOfMonth : 1;	   

	   
	   var additional_beneficiary = $(this).val();
	   additional_beneficiary = (additional_beneficiary) ? additional_beneficiary : 0;
	   var total_beneficiary_id = "total_beneficiary_" + index;
	   var total_beneficiary = $("#"+total_beneficiary_id).val();
	   total_beneficiary = (total_beneficiary) ? total_beneficiary : 0;
	   
	   var totalAmount = 0;
	   totalAmount = (eval(total_beneficiary) + eval(additional_beneficiary)) * eval(monthlyAmount)  * eval(numberOfMonth);	
	   
	   var totalAmount_id = "totalAmount_" + index;
	   $("#"+totalAmount_id).html(totalAmount);
	   
	   
      // console.log("additional_beneficiary = "+additional_beneficiary);
	 //  console.log("total_beneficiary = "+total_beneficiary);
	   
	   // sum of amount
	   var sumTotalAmount = 0;
	   var sumTotalAmount_Ids;
	   $('.totalAmount').each(function (index, value) { 
			  //console.log('div' + index + ':' + $(this).attr('id')); 
			  sumTotalAmount_Ids = $(this).attr('id');
			  sumTotalAmount_Ids = $("#"+sumTotalAmount_Ids).html();
			  sumTotalAmount_Ids = (sumTotalAmount_Ids) ? sumTotalAmount_Ids : 0;
			  sumTotalAmount += eval(sumTotalAmount_Ids);	  
	   });
   
	   $("#sumTotalAmount").html(sumTotalAmount);
	   
	   // sum of addition Beneficial
	   var sumAdditionBeneficial = 0;
	   var sumAdditionBeneficial_Ids;
	   $('.additionBeneficial').each(function (index, value) { 
			  //console.log('div' + index + ':' + $(this).attr('id')); 
			  sumAdditionBeneficial_Ids = $(this).attr('id');
			  sumAdditionBeneficial_Ids = $("#"+sumAdditionBeneficial_Ids).val();
			  sumAdditionBeneficial_Ids = (sumAdditionBeneficial_Ids) ? sumAdditionBeneficial_Ids : 0;
			  sumAdditionBeneficial += eval(sumAdditionBeneficial_Ids);	  
	   });
   
	   $("#sumAdditionalBeneficiary").html(sumAdditionBeneficial);
	   
	   
	   
//       console.log("additional_beneficiary = "+additional_beneficiary);
			  
	   
    });
	
	
});
</script>