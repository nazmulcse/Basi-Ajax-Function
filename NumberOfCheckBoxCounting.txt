 $(document).ready(function () {
        $("input#preposition_naming").on('click', function () {
            $count=$("input#preposition_naming:checked").length;
            $('#PreScore').html($count);
        });
        $("input#ExpBody").on('click', function () {
            $count=$("input#ExpBody:checked").length;
            $('#ExpBodyId').html($count);
        });
        $("input#ExpSinglPic").on('click', function () {
            $count=$("input#ExpSinglPic:checked").length;
            $('#ExpSinglPicId').html($count);
        });
        $("input#ExpSinglObj").on('click', function () {
            $count=$("input#ExpSinglObj:checked").length;
            $('#ExpSinglObjId').html($count);
        });
        $("input#ExpVerbsCard").on('click', function () {
            $count=$("input#ExpVerbsCard:checked").length;
            $('#ExpVerbsCardId').html($count);
        });

        $("input#CompBody").on('click', function () {
            $count=$("input#CompBody:checked").length;
            $('#CompBodyId').html($count);
        });
        $("input#CompSinglePic").on('click', function () {
            $count=$("input#CompSinglePic:checked").length;
            $('#CompSinglePicId').html($count);
        });
        $("input#CompSingleObj").on('click', function () {
            $count=$("input#CompSingleObj:checked").length;
            $('#CompSingleObjId').html($count);
        });
        $("input#CompVerbCard").on('click', function () {
            $count=$("input#CompVerbCard:checked").length;
            $('#CompVerbCardId').html($count);
        });
        $("input#CompTwoKeyword").on('click', function () {
            $count=$("input#CompTwoKeyword:checked").length;
            $('#CompTwoKeywordId').html($count);
        });
        $("input#CompPrePosition").on('click', function () {
            $count=$("input#CompPrePosition:checked").length;
            $('#CompPrePositionId').html($count);
        });
    })