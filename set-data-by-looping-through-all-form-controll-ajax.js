factory_chosen_id_field.on('change', function () {
                var factory_chosen_id = factory_chosen_id_field.val();

                if (factory_chosen_id.length > 0) {

                    $('[name="factory_id"]').val(factory_chosen_id);

                    $.ajax({
                        dataType: 'json',
                        type: "GET",
                        url: "/fd/application/getFactoryIdWiseFactoryInformationForCategoryChange/" + factory_chosen_id,
                        cache: false,
                        success: function (response) {

                            //Factory Information
                            //link: https://stackoverflow.com/a/4260329
                            for(var elem_name in response) {
                                $('.form-control').each(function (index, value) {
                                    var this_elem = $(this);

                                    if( this_elem.attr('name') == elem_name ) {
                                        this_elem.val(response[elem_name]);
                                    }
                                });
                            }

                            change_category_form.slideDown();
                        }
                    });

                } else {
                    change_category_form.slideUp();
                }
            });